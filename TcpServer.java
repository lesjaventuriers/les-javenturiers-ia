import java.net.*;
import java.util.Scanner;
import java.io.*;

public class TcpServer {

	public static void main(String[] args) throws SocketException, IOException, UnknownHostException {
		
		boolean exit = false;
		int port = Integer.parseInt(args[0]);
		ServerSocket srv = new ServerSocket(port);
		Socket socket = srv.accept();
		
		while(!exit){
			
			/* Le serveur lit une chaîne de caractères entrée au clavier et l'envoie vers le client */
			BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			System.out.println("Entrez le message que vous souhaitez envoyer au client (ou stop pour quitter) :");
			Scanner sc = new Scanner(System.in);
			String s = sc.nextLine();
			wr.write(s + "\n");
			wr.flush();
			if(s.equals("stop")){
				exit = true;
				sc.close();
				srv.close();
			} /*else {
				wr.write(s + "\n");
				wr.flush();
			}*/
			
			/* le serveur reçoit la chaîne, l'affiche à l'écran et on retourne à l'étape 1 */
			//Socket socket = new Socket(args[0], port);
			BufferedReader rd = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			System.out.println("Message recu par le client : " + rd.readLine());
			
		}
	}
	
}
