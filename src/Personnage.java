import java.awt.Point;
import java.util.ArrayList;

import labyrinthe.Labyrinthe;


public class Personnage {

	private Labyrinthe laby;
	private int posX;
	private int posY;
	private int bieres;
	private int frites;
	private int moules;
	private int numJoueur;

	public Personnage(int num,Labyrinthe l,int x, int y){
		this.numJoueur=num;
		this.laby=l;
		this.posX=x;
		this.posY=y;
		this.frites=0;
		this.bieres=0;
		this.moules=0;
	}

	public ArrayList<Object[]> positionAccessibleSansObjet(){
		ArrayList<Object[]> liste = new ArrayList<Object[]>();
		String objet=null;

		if(this.laby.etreAccessible(this.posX+1, this.posY)){
			Object[] obj = new Object[3];

			obj[0]=new Point(this.posX+1,this.posY);
			obj[1]=objet;
			obj[2]="S";
			liste.add(obj);
		}


		if(this.laby.etreAccessible(this.posX, this.posY+1)){
			Object[] obj = new Object[3];

			obj[0]=new Point(this.posX,this.posY+1);
			obj[1]=objet;
			obj[2]="E";
			liste.add(obj);
		}

		if(this.laby.etreAccessible(this.posX-1, this.posY)){
			Object[] obj = new Object[3];

			obj[0]=new Point(this.posX-1,this.posY);
			obj[1]=objet;
			obj[2]="N";
			liste.add(obj);
		}


		if(this.laby.etreAccessible(this.posX, this.posY-1)){
			Object[] obj = new Object[3];

			obj[0]=new Point(this.posX,this.posY-1);
			obj[1]=objet;
			obj[2]="O";
			liste.add(obj);
		}

		return liste;
	}


	public ArrayList<Object[]> positionAccessibleFrites(){
		ArrayList<Object[]> liste = new ArrayList<Object[]>();
		if (frites>0){
			String objet="F";
			Object[] obj = new Object[3];


			if(this.laby.etreAccessible(this.posX+2, this.posY)){
				obj[0]=new Point(this.posX+2,this.posY);
				obj[1]=objet;
				obj[2]="EE";
				liste.add(obj);

			}

			if(this.laby.etreAccessible(this.posX, this.posY+2)){
				obj[0]=new Point(this.posX,this.posY+2);
				obj[1]=objet;
				obj[2]="NN";
				liste.add(obj);
			}
			
		
		
			if(this.laby.etreAccessible(this.posX-2, this.posY)){
				obj[0]=new Point(this.posX-2,this.posY);
				obj[1]=objet;
				obj[2]="OO";
				liste.add(obj);
			}
		
		
		
		
			if(this.laby.etreAccessible(this.posX, this.posY-2)){
				obj[0]=new Point(this.posX,this.posY-2);
				obj[1]=objet;
				obj[2]="SS";
				liste.add(obj);
			}
		
	
		}
		return liste;
	}


	public ArrayList<Object[]> positionAccessibleBieres(){
		ArrayList<Object[]> liste = new ArrayList<Object[]>();
		
		if(this.laby.etreAccessible(this.getPosX(), this.getPosY()-3))
		{
			if(this.laby.etreAccessible(this.getPosX(), this.getPosY()-2) && this.laby.etreAccessible(this.getPosX(), this.getPosY()-1))
			{
				Object[] obj = new Object[3];
				obj[0] = new Point(this.getPosX(), this.getPosY()-3);
				obj[1] = "B";
				obj[2] = "NNN";
			}
		}
		
		if(this.laby.etreAccessible(this.getPosX(), this.getPosY()+3))
		{
			if(this.laby.etreAccessible(this.getPosX(), this.getPosY()+2) && this.laby.etreAccessible(this.getPosX(), this.getPosY()+1))
			{
				Object[] obj = new Object[3];
				obj[0] = new Point(this.getPosX(), this.getPosY()+3);
				obj[1] = "B";
				obj[2] = "SSS";
			}
		}
		
		if(this.laby.etreAccessible(this.getPosX()-3, this.getPosY()))
		{
			if(this.laby.etreAccessible(this.getPosX()-2, this.getPosY()) && this.laby.etreAccessible(this.getPosX()-1, this.getPosY()))
			{
				Object[] obj = new Object[3];
				obj[0] = new Point(this.getPosX()-3, this.getPosY());
				obj[1] = "B";
				obj[2] = "OOO";
			}
		}
		
		if(this.laby.etreAccessible(this.getPosX()+3, this.getPosY()))
		{
			if(this.laby.etreAccessible(this.getPosX()+2, this.getPosY()) && this.laby.etreAccessible(this.getPosX()+1, this.getPosY()))
			{
				Object[] obj = new Object[3];
				obj[0] = new Point(this.getPosX()+3, this.getPosY());
				obj[1] = "B";
				obj[2] = "EEE";
			}
		}
		
		if(this.laby.etreAccessible(this.getPosX()-2, this.getPosY()-1))
		{
			if(this.laby.etreAccessible(this.getPosX(), this.getPosY()-1) && this.laby.etreAccessible(this.getPosX()-1, this.getPosY()-1))
			{
				Object[] obj = new Object[3];
				obj[0] = new Point(this.getPosX()-2, this.getPosY()-1);
				obj[1] = "B";
				obj[2] = "NOO";				
			}
			else if(this.laby.etreAccessible(this.getPosX()-1, this.getPosY()) && this.laby.etreAccessible(this.getPosX()-2, this.getPosY()))
			{
				Object[] obj = new Object[3];
				obj[0] = new Point(this.getPosX()-2, this.getPosY()-1);
				obj[1] = "B";
				obj[2] = "OON";						
			}
			else if(this.laby.etreAccessible(this.getPosX()-1, this.getPosY()) && this.laby.etreAccessible(this.getPosX()-1, this.getPosY()-1))
			{
				Object[] obj = new Object[3];
				obj[0] = new Point(this.getPosX()-2, this.getPosY()-1);
				obj[1] = "B";
				obj[2] = "ONO";		
			}
		}
		
		if(this.laby.etreAccessible(this.getPosX()-1, this.getPosY()-2))
		{
			if(this.laby.etreAccessible(this.getPosX(), this.getPosY()-1) && this.laby.etreAccessible(this.getPosX(), this.getPosY()-2))
			{
				Object[] obj = new Object[3];
				obj[0] = new Point(this.getPosX()-1, this.getPosY()-2);
				obj[1] = "B";
				obj[2] = "NNO";				
			}
			else if(this.laby.etreAccessible(this.getPosX()-1, this.getPosY()) && this.laby.etreAccessible(this.getPosX()-1, this.getPosY()-1))
			{
				Object[] obj = new Object[3];
				obj[0] = new Point(this.getPosX()-1, this.getPosY()-2);
				obj[1] = "B";
				obj[2] = "ONN";						
			}
			else if(this.laby.etreAccessible(this.getPosX(), this.getPosY()-1) && this.laby.etreAccessible(this.getPosX()-1, this.getPosY()-1))
			{
				Object[] obj = new Object[3];
				obj[0] = new Point(this.getPosX()-1, this.getPosY()-2);
				obj[1] = "B";
				obj[2] = "NON";		
			}
		}
		
		if(this.laby.etreAccessible(this.getPosX()+1, this.getPosY()-2))
		{
			if(this.laby.etreAccessible(this.getPosX(), this.getPosY()-1) && this.laby.etreAccessible(this.getPosX(), this.getPosY()-2))
			{
				Object[] obj = new Object[3];
				obj[0] = new Point(this.getPosX()+1, this.getPosY()-2);
				obj[1] = "B";
				obj[2] = "NNE";				
			}
			else if(this.laby.etreAccessible(this.getPosX()+1, this.getPosY()) && this.laby.etreAccessible(this.getPosX()+1, this.getPosY()-1))
			{
				Object[] obj = new Object[3];
				obj[0] = new Point(this.getPosX()+1, this.getPosY()-2);
				obj[1] = "B";
				obj[2] = "ENN";						
			}
			else if(this.laby.etreAccessible(this.getPosX(), this.getPosY()-1) && this.laby.etreAccessible(this.getPosX()+1, this.getPosY()-1))
			{
				Object[] obj = new Object[3];
				obj[0] = new Point(this.getPosX()+1, this.getPosY()-2);
				obj[1] = "B";
				obj[2] = "NEN";		
			}
			
			if(this.laby.etreAccessible(this.getPosX()+2, this.getPosY()-1))
			{
				if(this.laby.etreAccessible(this.getPosX()+1, this.getPosY()) && this.laby.etreAccessible(this.getPosX()+2, this.getPosY()))
				{
					Object[] obj = new Object[3];
					obj[0] = new Point(this.getPosX()+1, this.getPosY()-1);
					obj[1] = "B";
					obj[2] = "EEN";				
				}
				else if(this.laby.etreAccessible(this.getPosX(), this.getPosY()-1) && this.laby.etreAccessible(this.getPosX()+1, this.getPosY()-1))
				{
					Object[] obj = new Object[3];
					obj[0] = new Point(this.getPosX()+2, this.getPosY()-1);
					obj[1] = "B";
					obj[2] = "NEE";						
				}
				else if(this.laby.etreAccessible(this.getPosX()+1, this.getPosY()) && this.laby.etreAccessible(this.getPosX()+1, this.getPosY()-1))
				{
					Object[] obj = new Object[3];
					obj[0] = new Point(this.getPosX()+2, this.getPosY()-1);
					obj[1] = "B";
					obj[2] = "ENE";		
				}
			}
		}
		
		if(this.laby.etreAccessible(this.getPosX()-2, this.getPosY()+1))
		{
			if(this.laby.etreAccessible(this.getPosX(), this.getPosY()+1) && this.laby.etreAccessible(this.getPosX()-1, this.getPosY()+1))
			{
				Object[] obj = new Object[3];
				obj[0] = new Point(this.getPosX()-2, this.getPosY()+1);
				obj[1] = "B";
				obj[2] = "SOO";				
			}
			else if(this.laby.etreAccessible(this.getPosX()-1, this.getPosY()) && this.laby.etreAccessible(this.getPosX()-2, this.getPosY()))
			{
				Object[] obj = new Object[3];
				obj[0] = new Point(this.getPosX()-2, this.getPosY()+1);
				obj[1] = "B";
				obj[2] = "OOS";						
			}
			else if(this.laby.etreAccessible(this.getPosX()-1, this.getPosY()) && this.laby.etreAccessible(this.getPosX()-1, this.getPosY()+1))
			{
				Object[] obj = new Object[3];
				obj[0] = new Point(this.getPosX()-2, this.getPosY()+1);
				obj[1] = "B";
				obj[2] = "OSO";		
			}
		}
		
		if(this.laby.etreAccessible(this.getPosX()-1, this.getPosY()+2))
		{
			if(this.laby.etreAccessible(this.getPosX(), this.getPosY()+1) && this.laby.etreAccessible(this.getPosX(), this.getPosY()+2))
			{
				Object[] obj = new Object[3];
				obj[0] = new Point(this.getPosX()-1, this.getPosY()+2);
				obj[1] = "B";
				obj[2] = "SSO";				
			}
			else if(this.laby.etreAccessible(this.getPosX()-1, this.getPosY()) && this.laby.etreAccessible(this.getPosX()-1, this.getPosY()+1))
			{
				Object[] obj = new Object[3];
				obj[0] = new Point(this.getPosX()-1, this.getPosY()+2);
				obj[1] = "B";
				obj[2] = "OSS";						
			}
			else if(this.laby.etreAccessible(this.getPosX(), this.getPosY()+1) && this.laby.etreAccessible(this.getPosX()-1, this.getPosY()+1))
			{
				Object[] obj = new Object[3];
				obj[0] = new Point(this.getPosX()-1, this.getPosY()+2);
				obj[1] = "B";
				obj[2] = "SOS";		
			}
		}
		
		if(this.laby.etreAccessible(this.getPosX()+1, this.getPosY()+2))
		{
			if(this.laby.etreAccessible(this.getPosX(), this.getPosY()+1) && this.laby.etreAccessible(this.getPosX(), this.getPosY()+2))
			{
				Object[] obj = new Object[3];
				obj[0] = new Point(this.getPosX()+1, this.getPosY()+2);
				obj[1] = "B";
				obj[2] = "SSE";				
			}
			else if(this.laby.etreAccessible(this.getPosX()+1, this.getPosY()) && this.laby.etreAccessible(this.getPosX()+1, this.getPosY()+1))
			{
				Object[] obj = new Object[3];
				obj[0] = new Point(this.getPosX()+1, this.getPosY()+2);
				obj[1] = "B";
				obj[2] = "ESS";						
			}
			else if(this.laby.etreAccessible(this.getPosX(), this.getPosY()+1) && this.laby.etreAccessible(this.getPosX()+1, this.getPosY()+1))
			{
				Object[] obj = new Object[3];
				obj[0] = new Point(this.getPosX()+1, this.getPosY()+2);
				obj[1] = "B";
				obj[2] = "SES";		
			}
			
			if(this.laby.etreAccessible(this.getPosX()+2, this.getPosY()+1))
			{
				if(this.laby.etreAccessible(this.getPosX()+1, this.getPosY()) && this.laby.etreAccessible(this.getPosX()+2, this.getPosY()))
				{
					Object[] obj = new Object[3];
					obj[0] = new Point(this.getPosX()+1, this.getPosY()+1);
					obj[1] = "B";
					obj[2] = "EES";				
				}
				else if(this.laby.etreAccessible(this.getPosX(), this.getPosY()+1) && this.laby.etreAccessible(this.getPosX()+1, this.getPosY()+1))
				{
					Object[] obj = new Object[3];
					obj[0] = new Point(this.getPosX()+2, this.getPosY()+1);
					obj[1] = "B";
					obj[2] = "SEE";						
				}
				else if(this.laby.etreAccessible(this.getPosX()+1, this.getPosY()) && this.laby.etreAccessible(this.getPosX()+1, this.getPosY()+1))
				{
					Object[] obj = new Object[3];
					obj[0] = new Point(this.getPosX()+2, this.getPosY()+1);
					obj[1] = "B";
					obj[2] = "ESE";		
				}
			}
		}
		
		return liste;
	}

	/*public boolean deplacementOk(int x, int y){

	}*/


	public void recuperBiere(){

	}


	public void recupererFrite(){

	}

	public void recupererMoule(){

	}

	public void utiliserFrite(){

	}

	public void utiliserBiere(){

	}


	public Labyrinthe getLaby() {
		return laby;
	}


	public int getPosX() {
		return posX;
	}


	public void setPosX(int posX) {
		this.posX = posX;
	}


	public int getPosY() {
		return posY;
	}


	public void setPosY(int posY) {
		this.posY = posY;
	}


	public int getBieres() {
		return bieres;
	}


	public void setBieres() {
		this.bieres++;
	}


	public int getFrites() {
		return frites;
	}


	public void setFrites() {
		this.frites++;
	}


	public int getMoules() {
		return moules;
	}


	public void setMoules() {
		this.moules++;
	}




}