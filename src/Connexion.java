import java.io.IOException;
import java.net.*;
import java.io.*;

public class Connexion {

	//attribut connexion
	InetAddress address;
	int port;
	Socket sock;
	
	//attribut de flux
	InputStream in;
	InputStreamReader readin;
	OutputStream out;
	
	//attributs de gestion de string
	BufferedReader buff;
	PrintWriter pw;
	
	public Connexion(int port, String ip){
		try {
			
			this.port = port;
			this.address = InetAddress.getByName(ip);
			
			this.sock = new Socket(address,this.port);
			
			this.out = sock.getOutputStream();
			this.in = sock.getInputStream();
			
			this.readin = new InputStreamReader(in);
			this.buff = new BufferedReader(this.readin);

			this.pw = new PrintWriter(out, true);
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public String recevoirDonnees(){

		try {
			String ligne = this.buff.readLine();
			return ligne;
		} catch (IOException e) {
			System.out.println(e);
			return null;
		}
	}
	
	public void envoyerDonnees(String s){
		
		this.pw.println(s);
		
	}
	
	
	public void fermerConnexion(){
		
		try {
			this.buff.close();
			this.readin.close();
			this.in.close();
			this.out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
