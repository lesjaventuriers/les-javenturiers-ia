package labyrinthe;
import java.awt.*;
import java.util.*;


public class Labyrinthe {

	private String[][] tab ;
	private ArrayList<Point> posJoueurs ;
	private int nbJoueurs ;
	
	public Labyrinthe(String lab) {
		//Decoupe la chaine en 3
		String[] s = lab.split("/") ;
		//recupere les dimensions du labyrinthe
		String[] coo = s[0].split("x") ;
		int nbCol = Integer.parseInt(coo[1]) ;
		int nbL = Integer.parseInt(coo[0]);
		//remplissage du tableau
		tab = new String[nbL][nbCol] ;
		String[] cases = s[1].split("-") ;
		int indice =0 ;
		for(int i=0 ; i<nbL ; i++) {
			for(int j=0 ; j<nbCol ; j++) {
				tab[i][j] = cases[indice] ;
				indice ++ ;
			}
		}
		//nombre de joueurs + coordonnees
		String[] joueurs = s[2].split("-") ;
		nbJoueurs = Integer.parseInt(joueurs[0]) ;
		posJoueurs = new ArrayList<Point>(nbJoueurs) ;
		for(int i=0 ; i<nbJoueurs ; i++)
			posJoueurs.add(i, new Point(Integer.parseInt(joueurs[i+1].split(",")[0]), Integer.parseInt(joueurs[i+1].split(",")[1]))) ;
	}
	
	public String[][] getLabyrinthe() {
		return tab ;
	}
	
	public int getNbJoueurs() {
		return nbJoueurs ;
	}
	
	public ArrayList<Point> getJoueurs() {
		return posJoueurs ;
	}
	
	public ArrayList<Point> getBonus(String s) {
		ArrayList<Point> biere = new ArrayList<Point>() ;
		if(s.equals("B") || s.equals("F")) {
			for(int i=0 ; i<tab.length ; i++) {
				for(int j=0 ; j<tab[i].length ; j++) {
					if(tab[i][j].equals(s))
						biere.add(new Point(i, j)) ;
				}
			}
		}
		return biere ;
	}
	
	public ArrayList<Moule> getMoules() {
		ArrayList<Moule> moules = new ArrayList<Moule>() ;
		for(int i=0 ; i<tab.length ; i++) {
			for(int j=0 ; j<tab[i].length ; j++) {
				if(estUnEntier(tab[i][j]))
					moules.add(new Moule(new Point(i, j), Integer.parseInt(tab[i][j]))) ;	
			}
		}
		return moules ;
	}
	
	public boolean estUnEntier(String chaine) {
		try {
			Integer.parseInt(chaine);
		} catch (NumberFormatException e){
			return false;
		}
 
		return true;
	}
	
	public boolean etreAccessible(int x, int y) {
		if(x < 0 || y < 0 || x > tab.length || y > tab[x].length)
			return false;
		if(tab[x][y].equals("D"))
			return false ;
		return true ;
	}
	
	public String contient(int x, int y){
		return tab[x][y];
	}
	
	public double tauxDunes() {
		int d = 0 ;
		for(int i=1 ; i<tab.length-1 ; i++)
			for(int j=1 ; j<tab[i].length-1 ; j++) {
				if(!etreAccessible(i, j))
					d++ ;
			}
		int nbCases = tab.length*tab[0].length ;
		return d/nbCases ;
	}
	
	public int distance(int x1, int y1, int x2, int y2) {
		return Math.abs(x2-x1)+Math.abs(y2-y1) ;
	}
	
	
}
