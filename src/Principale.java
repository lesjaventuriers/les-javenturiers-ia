import java.awt.Point;
import java.util.ArrayList;
import java.util.Scanner;
import labyrinthe.Labyrinthe;

public class Principale {

	private static String nom = "Les Jav'enturiers";
	public static int numero = 0;
	
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("IP ?");
		String ip = sc.nextLine();
		
		System.out.println("Port ?");
		int port = sc.nextInt();
		Connexion co = new Connexion(port,ip);
		
		String map = "";
	
		co.envoyerDonnees(nom);
		String numEquipe = "";
		
		do{
			numEquipe = co.recevoirDonnees();
		}while(numEquipe == null);
		numero = Integer.parseInt(numEquipe);
		System.out.println(numEquipe);
		
		
		sc.close();
		//gestion IA
		while(true)
		{
			map = co.recevoirDonnees();
			System.out.println(map);
			if(map.equals("FIN"))
				break;
			
			Labyrinthe l = new Labyrinthe(map);
			Personnage nous = new Personnage(numero,l,(int)l.getJoueurs().get(numero).getX(),(int) l.getJoueurs().get(numero).getY());
			
			//CALCULS IA
			
			Object[] resCalculs = calculsIA(l.getJoueurs().get(numero),l,nous);
			
			//[ POINT, [objet utilise], chemin sous forme de String]
			
			String envoi = "";
			
			if(resCalculs[1]==null){
				envoi = (String) resCalculs[2];
			}else{
				if(resCalculs[1] == "F"){
					envoi = "F-" +(String) resCalculs[2];
				}
				else{
					envoi = "B-";
					for(int i = 0;i<2;i++){
						envoi += ((String) resCalculs[2]).charAt(i) + "-";
					}
					envoi += ((String)resCalculs[2]).charAt(2);
				}
			}
			
			co.envoyerDonnees(envoi);
		}
		
		System.out.println("Fin du programme");
		
		co.fermerConnexion();
	}
	
	public static Object[] calculsIA(Point pos, Labyrinthe l, Personnage nous){
		//on calcule toutes les cases ou on peut aller...
		ArrayList<Object[]> list = new ArrayList<Object[]>();
		list = nous.positionAccessibleSansObjet();
		/*
		for(Object[] i : nous.positionAccessibleFrites()){
			list.add(i);
		}
		
		for(Object[] i : nous.positionAccessibleBieres()){
			list.add(i);
		}*/
		//on teste chaque sous-IA l'une apr�s l'autre
		Object[] res = null;
		
		res = IA.chercherMoules(list,l);
		System.out.println("moules : " + res);
		if(res!= null){
			return res;
		}
		
		res = IA.ChercherBonus(list, l);
		System.out.println("bonus" + res);
		if(res !=null){
			return res;
		}
		
		res = IA.ChercherParDefaut(list, l);
		System.out.println("defaut" + res);
		if(res != null){
			return res;
		}
		
		return res;
	}
}
