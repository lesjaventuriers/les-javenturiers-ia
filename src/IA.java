import java.awt.Point;
import java.util.ArrayList;

import labyrinthe.Labyrinthe;

public class IA {

	/**
	 * On prend en argument:
	 * 		un ArrayList<Object[]>
	 * 		contenant : [ POINT, [objet utilise], chemin sous forme de String]
	 *
	 *		on retourne Object[] en question
	 */
	
	
	public static Object[] chercherMoules(ArrayList<Object[]> tabCase, Labyrinthe l){
		// on check si y a des moules et on prend celle qui vaut le plus
		ArrayList<Object[]> list = new ArrayList<Object[]>();//tableau [POINT, valeurMoule , objet , chemin]
		
		for(Object[] i : tabCase){
			try{
				Integer.parseInt(l.contient((int)((Point)i[0]).getX(), (int) ((Point)i[0]).getY()));
				list.add( new Object[] {(Point) i[0],Integer.parseInt(l.contient((int)((Point)i[0]).getX(), (int) ((Point)i[1]).getY())) , (String) i[1] , (String)i[2] } );
			}catch(NumberFormatException e){
				
			}
		}
		if(!list.isEmpty()){
			Object[] max = list.get(0);
			for(Object[] i : list){
				if((int)i[1]>(int)max[1]){
					max =  i;
				}
			}
			return (new Object[] {max[0], max[2] , max[3]});
		}
		return null;
	}
	
	public static Object[] ChercherBonus(ArrayList<Object[]> tabCase, Labyrinthe l){
		//on check pour aller chercher les bonus
		// on a list vide
		
		//si + de dunes => FRITES
		//si - de dunes => Biere
		double tauxDune = l.tauxDunes();
		String bonus = "F";
		if(tauxDune<0.5){
			bonus = "B";
		}
		for(Object[] i : tabCase){
			if(l.contient( (int)((Point)i[0]).getX(), (int)((Point)i[0]).getY()).equals(bonus)){
				return i;
			}
		}
		return null;
	}
	
	public static Object[] ChercherParDefaut(ArrayList<Object[]> tabCase, Labyrinthe l){
		//choix random
		int a = (int) (Math.random()*(tabCase.size()));
		return tabCase.get(a);
	}
}
