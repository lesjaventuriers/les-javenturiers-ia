import java.net.*;
import java.util.Scanner;
import java.io.*;

public class TcpClient {
	
	public static void main(String[] args) throws SocketException, IOException, UnknownHostException {
		
		boolean exit = false;
		int port = Integer.parseInt(args[1]);
		Socket socket = new Socket(args[0], port);
		
		while(!exit){
			/* le client reçoit la chaîne, l'affiche à l'écran, ... */
			BufferedReader rd = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			/*if(rd.readLine().equals("stop")){
				System.out.println("Fermeture du programme");
				exit = true;
			} else {*/
				System.out.println("Message recu par le serveur : " + rd.readLine());
			//}
			
			/* ... puis lit une chaîne de caractères entrée au clavier pour l'envoyer vers le serveur. */
			BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			System.out.println("Entrez le message que vous souhaitez envoyer au serveur (ou stop pour quitter) :");
			Scanner sc = new Scanner(System.in);
			String s = sc.nextLine();
			if(s.equals("stop")){
				exit = true;
				sc.close();
				socket.close();
			} else {
				wr.write(s + "\n");
				wr.flush();
				/*sc.close();
				socket.close();*/
			}
		}
	}
	
}
